/** TUM-EES GetContent( filename )
 * @brief			Führt ajax-Anfrage aus und aktuallisiert den Inhalt. Die Datei
 *						filename wird im Ordner ./sites/ gesucht und deren Inhalt im div
 *						content wiedergegeben.
 * @log				11.05.2014 Grundfunktion aufgebaut
 * @depends		jQuery 1.4.2+
 * @autor			Max F. Horsche (MFH)
 * @date			11.05.2014
 */
function getContent ( contentFile ) {
	console.log( contentFile );
	// ajax-Abfrage
	$.ajax({
		type: "GET",
		url: "Sites/" + contentFile + ".php",
		processData: false,
 	 	dataType: "html",
		// ajax-Abfrage fehlgeschlagen
		error: function(e, b, error) { 
			alert(e.respone);
		},
		// ajax-Abfrage erfolgreich
		success: function( html ) {
			$( "div#content-wrapper div.documentBody" ).html( html );
		}
	});
}