/** TUM-EES Navigation
 * @brief			Animiert Menüleiste und ermittelt alle übergeordneten 
 *						Menüpunkte woraus die Navigations-Übersicht erstellt
 *						wird.
 * @log				05.05.2014 Grundfunktion aufgebaut
 *						11.05.2014 Wird nur ausgeführt wenn neues Element an-
 *						gewählt. Liest attr( file ) des gewählten a-Tags und
 *						führt Funktion getContent( file ) aus.
 * @depends		jQuery 1.4.2+
 * @autor			Max F. Horsche (MFH)
 * @date			11.05.2014
 */
jQuery(document).ready(function(){
	// Erkenne Klick auf Menüpunkt
	$( "ul.navigationlist a" ).click( function () {
		// Nur wenn neues Element geklickt
		if ( !$(this).hasClass( "selected" ) ) {
			// Unterscheide aufklappbares Elemente
			if ( $(this).parent( "li" ).hasClass( "childs" ) ){
				// Klappe Menüpunkte auf/zu
				$(this).parent( "li" ).toggleClass( "active" );
				$(this).next( "ul" ).slideToggle();
			}else{
				// ajax-Abfrage für neuen Inhalt ausführen
				if ( $(this).attr( "file" ) ) {
					getContent( $(this).attr( "file" ) );
				};
				// Navigationspunkt in Menü erkennen und als "selected" markieren
				$( "ul.navigationlist a.selected" ).removeClass( "selected" );
				$(this).addClass( "selected" );
				// Navigations Übersicht neu erstellen
				$( "div.navigationview li" ).remove();
				$( "div.navigationview ul" ).append( '<li><strong>' + $(this).text() + '</strong></li>' );
				$(this).parents( "ul" ).each( function () {
					var navigation = $(this).parent( "li" ).find( "a" ).first().text();
					if ( navigation ) {
						// Übergeordnete Navigationspunkte erkennen und als "selected" markieren
						$(this).parent( "li" ).first( "a" ).addClass( "selected" );
						// Übergeordnete Punkte in Navigations Übersicht erstellen
						$( "div.navigationview ul" ).find( "li" ).first().before( '<li>' + navigation + '</li>' );
					};
				});
			};
		};
	});
});