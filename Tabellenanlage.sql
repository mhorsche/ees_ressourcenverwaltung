CREATE database IF NOT EXISTS mess_db;

USE mess_db;

CREATE TABLE IF NOT EXISTS userGroups (
     id INT NOT NULL AUTO_INCREMENT,
     name VARCHAR(50) NOT NULL UNIQUE,
	 requestRight BOOL NOT NULL,
	 approvalRight BOOL NOT NULL,
     PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS users (
     id INT NOT NULL AUTO_INCREMENT,
	 name VARCHAR(100) NOT NULL,
	 AD_name VARCHAR(100) NOT NULL UNIQUE,
     userID INT NOT NULL,
	 groupID INT NOT NULL,
     PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS groupMembership (
     userID INT NOT NULL,
	 groupID INT NOT NULL,
     PRIMARY KEY (userID,groupID),
	 FOREIGN KEY (userID) REFERENCES users(id),
	 FOREIGN KEY (groupID) REFERENCES userGroups(id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS projects (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	description VARCHAR(1024),
	PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS machines (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	description VARCHAR(255),
	PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS channelTypes (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	description VARCHAR(255),
	PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS serialTypes (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS batteryTypes (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	description VARCHAR(255),
	PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS proposals (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	description1 VARCHAR(1024),
	description2 VARCHAR(1024),
	description3 VARCHAR(1024),
	description4 VARCHAR(1024),
	PRIMARY KEY (id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS booking (
	measureID INT NOT NULL,
	channelID INT NOT NULL,
	startDate DATETIME NOT NULL,
	endDate DATETIME NOT NULL,
	approved BOOL,
	PRIMARY KEY (measureID,channelID),
	FOREIGN KEY (measureID) REFERENCES measures(id),
	FOREIGN KEY (channelID) REFERENCES channels(id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS measures (
     id INT NOT NULL AUTO_INCREMENT,
	 userID INT NOT NULL,
	 startDate DATETIME NOT NULL,
	 endDate DATETIME NOT NULL,
	 approved BOOL,
	 previousID INT,
	 nextID INT,
	 projectID INT NOT NULL,
	 serialTypeID INT NOT NULL,
	 batteryTypeID INT NOT NULL,
	 proposalID INT NOT NULL,
     PRIMARY KEY (id),
	 FOREIGN KEY (userID) REFERENCES users(id),
	 FOREIGN KEY (projectID) REFERENCES projects(id),
	 FOREIGN KEY (serialTypeID) REFERENCES serialTypes(id),
	 FOREIGN KEY (batteryTypeID) REFERENCES batteryTypes(id),
	 FOREIGN KEY (proposalID) REFERENCES proposals(id)
) ENGINE=myISAM;

CREATE TABLE IF NOT EXISTS channels (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	machineID INT NOT NULL,
	typeID INT NOT NULL,
	value1 INT,
	value2 INT,
	value3 INT,
	value4 INT,
	PRIMARY KEY (id),
    FOREIGN KEY (machineID) REFERENCES machines(id),
	FOREIGN KEY (typeID) REFERENCES channelTypes(id)
) ENGINE=myISAM;

ALTER TABLE `mess_db`.`batterytypes` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`booking` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`channels` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`channeltypes` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`groupmembership` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`machines` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`measures` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`projects` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`proposals` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`serialtype` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`usergroups` CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `mess_db`.`users` CHARACTER SET utf8 COLLATE utf8_general_ci;