﻿<?php
/** index.php
 * @brief			index der gesamten Webseite
 * @log				11.05.2014 Navigation eingefügt (statisch)
 * 					05.05.2014 CSS-Sheets eingefügt
 * 					27.04.2014 Datei angelegt
 * @autor			Max F. Horsche (MFH)
 * @date			11.05.2014
 */
include("global.vars.php")
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>EES Ressourcen-Verwaltung</title>
  <link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.basic.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.header.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.personaltools.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.navigationlist.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.newslist.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.content.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/tum.footer.css" media="screen">
  
  <script language="JavaScript" type="text/javascript" src="plugins/jquery-2.1.0.min.js"></script>
  <script language="JavaScript" type="text/javascript" src="plugins/jquery-ui-1.10.4.custom.min.js"></script>
  <script language="JavaScript" type="text/javascript" src="plugins/jquery.multiselect.min.js"></script>
  
  <script language="JavaScript" type="text/javascript" src="scripts/mfh.getContent.js"></script>
  <script language="JavaScript" type="text/javascript" src="scripts/mfh.navigationView.js"></script>
</head>

<body>
<script>
jQuery(document).ready(function(){
	getContent ( "Startseite" );
});
</script>
<div id="visual-wrapper">
	<!--  START: Kopfzeile -->
  <div id="header">
    <h1><a href="">Technische Universität München</a></h1>
    <h2>Fakultät Elektrotechnik und Informationstechnik</h2>
    <p id="subclaim"><a href="">www.ei.tum.de</a></p>
    <p id="foto">Hallo</p>
  </div>
  <!--  START: Kopfzeile -->
  <!--  START: Persönliche Einstellungen -->
  <!--<div id="personaltools">
    <h3>Persönlicher Status und Werkzeuge</h3>
    <div>personaltools1</div>
  </div>-->
  <!--  START: Persönliche Einstellungen -->
  <!--  START: Hauptseite -->
  <div id="main-wrapper">
  	<!--  START: Inhalt -->
    <div id="content">
    	<!--  START: Navigationsleiste -->
    	<div class="navigationview">
        <ul>
          <li><strong>Startseite</strong></li>
        </ul>
      </div>
      <!--  ENDE: Navigationsleiste -->
      <!--  START: Seiteninhalt -->
      <div id="content-wrapper">
      	<div class="divider"><hr /></div>
        <div class="documentBody">
        </div>
        <!--<div class="toolline"><img src="./pic/print.png" width="12" height="16" alt="drucken"><a href="">Druckansicht</a></div>-->
      </div>
      <!--  ENDE: Seiteninhalt -->
    </div>
    <!--  ENDE: Inhalt -->
    <!--  START: Rechte Sidebar -->
    <!--<div id="news">
    	<div class="portlet_no_box"><img src="./pic/TUMEIEES_Logo.png" width="80" height="71" alt="">
        <div></div>
        <div>
          <h3>Kanalverwaltung</h3>
          <p> <b>Entwurfsversion:</b><br>
            v0.1,<br>
            max.horsche@tum.de </p>
        </div>
      </div>
      <div class="portlet_box"> 
        <div class="csc-header csc-header-n1">
          <h3 class="csc-firstHeader">Aktuelles</h3>
        </div>
        <h4>07.04.2014</h4>
        <p><a href="" title="Einsicht zur Prüfung Photovoltaische Inselsysteme">Einsicht zur Prüfung Photovoltaische Inselsysteme</a></p>
        <h4>17.03.2014</h4>
        <p><a href="" title="Der 3. Batteriestammtisch München zum Thema Batterieproduktion">Der 3. Batteriestammtisch München zum Thema Batterieproduktion</a></p>
        <h4>08.01.2014</h4>
        <p><a href="" title="2. Münchner Batteriestammtisch">2. Münchner Batteriestammtisch</a></p>
        <h4>02.10.2013</h4>
        <p><a href="" title="Hauptseminar Themen online">Hauptseminar Themen online</a></p>
      </div>
    </div>-->
    <!--  ENDE: Rechte Sidebar -->
    <!--  START: Linke Sidebar -->
    <div id="navigation">
      <h3 class="navigationlist">&nbsp;</h3>
      <ul class="navigationlist">
        <li><a file="Startseite" class="selected">Startseite</a></li>
        <li class="childs"><a>Messungen</a>
          <ul style="display: none;">
          	<li class="childs"><a>Kanalübersicht</a>
              <ul style="display: none;">
                <li><a file="Wochenansicht">Wochenansicht</a></li>
                <li><a file="Wochenansicht">Monatsansicht</a></li>
              </ul>
            </li>
            <li class="childs"><a>Eintragen</a>
            	<ul style="display: none;">
              	<li><a file="NeueMessreihe">Check-Up</a></li>
                <li><a file="NeueMessreihe">Messreihe</a></li>
              </ul>
            </li>
            <li><a>Bearbeiten</a></li>
          </ul>
        </li>
        <li class="childs"><a>Administration</a>
        	<ul style="display: none;">
          	<li><a>Einstellungen</a></li>
            <li><a>Whatever</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!--  ENDE: Linke Sidebar -->
  </div>
  <!--  ENDE: Hauptseite -->
  <!--  START: Footer -->
  <div id="footer">
    TUM.EI.EES - Lehrstuhl für Elektrische Energiespeichertechnik
    <div class="right">
      <!--<ul class="footer">
        <li><a href="">Datenschutz</a></li>
        <li><a href="">Impressum</a></li>
        <li><a href="">About</a></li>
      </ul>-->
    </div>
  </div>
  <!--  ENDE: Footer -->
</div>
</body>
</html>
